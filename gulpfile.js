var gulp = require('gulp')
var less = require('gulp-less');//less插件
var path = require('path');
var plumber = require('gulp-plumber');//获取报错句柄，防止任何构建错误后 导致gulp挂机的问题
var notify = require('gulp-notify'); //拿取报错信息


gulp.task('testLess', function () {
  //return gulp.src('./less/**/*.less') 所有Less编译
  return gulp.src('./less/less.less')
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))  //加上这插件的功能是防止gulp-less报错后 task挂掉的问题    
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./css'));
});

//监控文件
gulp.task('watch', function() {
   gulp.watch('./less/**/*.less', ['testLess']);
});

//默认任务，启动时仅在目录下键入gulp即可 不用再加任务名
gulp.task('default',['watch']); //定义默认任务
